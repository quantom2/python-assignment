"""
# 1
num = int(input('Enter the number: '))
count = 0
cnum = num
if cnum != 0:
    while cnum != 0:
        cnum //= 10
        count += 1
else:
    count = 1
num1 = num % 10
tempnum = num
if count == 4:
    print("Thousand's place: ", num1)
    tempnum = tempnum // 10
    count -= 1
    num1 = tempnum % 10
if count == 3:
    print("Hundred's place: ", num1)
    tempnum = tempnum // 10
    count -= 1
    num1 = tempnum % 10
if count == 2:
    print("Ten's place: ", num1)
    tempnum = tempnum//10
    count -= 1
    num1 = tempnum % 10
if count == 1:
    print("One's place: ", num1)
else:
    print("One's place: ", tempnum)
"""
"""
# 2

num1 = int(input('Enter the first number: '))
num2 = int(input('Enter the second number: '))
def count(number):
    count = 0
    if number != 0:
        while number != 0:
            number //= 10
            count += 1
    else:
        count = 1
    return count

count1 = count(num1)
count2 = count(num2)
if count1 != count2:
    print("both numbers are not mirror image")
else:
    mirror = 0
    while num2 > 0:
        mirror *= 10
        mirror += num2 % 10
        num2 //= 10
    if mirror == num1:
        print("both numbers are mirror image")
    else:
        print("both numbers are not mirror image")
"""
"""
# 3
string = input('Enter string to be reversed: ')
print('The given string is: ' + string)
print('The reverse string is: ' + string[-1::-1])
"""
"""
# 4
string = input('Enter string to find palindrome: ')
middle = len(string)//2
notpalind = 0
m = -1
for i in range(middle):
    if string[i] != string[m]:
        notpalind = 1
        break
    else:
        notpalind = 0
    m -= 1
if notpalind == 1:
    print('Given string {} is not palindrome'.format(string))
else:
    print('Given string {} is palindrome'.format(string))
"""
"""
# 5
num1 = int(input('Enter first number: '))
num2 = int(input('Enter second number: '))


def find_num(number1, number2):
    numbers = []
    count = 0
    for number1 in range(number1, number2+1):
        if number1 % 10 == 9:
            count += 1
            numbers.append(number1)            
        else:
            pass
    print('number of 9 values: ', count)
    print('list of 9 numbers: ', numbers)


if num1 <= num2:
    find_num(num1, num2)
else:
    find_num(num2, num1)
"""
"""
# 6
string = input("Enter string: ")
cstring = string.upper()
rows = len(string)
k = 0

for i in range(1, rows + 1):
    for space in range(1, (rows - i) + 1):
        print(end="  ")

    while k != (2*i-1):
        print(cstring[i-1]+" ", end='')
        k += 1

    k = 0
    print()
"""
"""
# 7
number = int(input('Enter the number: '))
if number % 3 == 0:
    print('Given number {} is divisible by 3'.format(number))
else:
    print('Given number {} is not divisible by 3'.format(number))
"""
"""
# 8
number = int(input('Enter the number: '))
List = []
for multiple in range(1, number+1):
    if number % multiple == 0:
        List.append(multiple)
    else:
        pass
print(List)
"""
"""
# 9
number = int(input('Enter the number: '))
count = 1
for num in range(2, number+1):
    if number % num == 0:
        count += 1
    else:
        pass
if count == 2:
    print('Given number {} is prime number'.format(number))
else:
    print('Given number {} is not prime number'.format(number))
"""
"""
# 10
List = []
for i in range(0, 2):
    List.append(int(input()))
find = int(input('Enter the value to find in the range: '))


def find_bet(num1, num2, find_num):
    if find_num in range(num1, num2):
        print('number is in the range')
    else:
        print('Number is not in range')


if List[0] < List[1]:
    find_bet(List[0], List[1], find)
else:
    find_bet(List[1], List[0], find)
"""

























