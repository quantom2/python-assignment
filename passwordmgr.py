class BasePasswordManager:
    old_passwords = list()

    def __init__(self):
        self.old_passwords.append('abc')

    def get_password(self):
        return self.old_passwords[-1]

    def is_correct(self, curr_pwd):
        if self.get_password() == curr_pwd:
            return True
        else:
            return False


class PasswordManager(BasePasswordManager):

    def set_password(self, curr_pwd):
        if len(curr_pwd) >= 6 and self.get_level(curr_pwd) == 2 and not self.is_correct(curr_pwd):
            self.old_passwords.append(curr_pwd)
        else:
            print("password cannot be updated123")

    def get_level(self, curr_pwd):
        alpha = digit = sp_ch = 0
        for ch in curr_pwd:
            if ch.isalpha():
                alpha += 1
            elif ch.isdigit():
                digit += 1
            else:
                sp_ch += 1
        if alpha > 0 and digit > 0 and sp_ch > 0:
            return 2


obj1 = PasswordManager()
pwd = input('Enter the password')
if obj1.get_level(pwd) == 2:
    obj1.set_password(pwd)
    print(obj1.old_passwords)
else:
    print('password cannot be updated')
